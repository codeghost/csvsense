/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse.column.definition;

import java.lang.reflect.InvocationTargetException;

import uk.co.codeghost.csvsense.parse.column.Column;

/**
 * <p>
 * Interface to be implemented by objects that determine if a String can be
 * converted into the {@link Column} type this definition represents and to
 * provide a constructor to do so.
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Nov 1, 2012
 */
public interface ColumnDefinition<T extends Column<?>> {

	/**
	 * This method should check the String value for conformity to be held by
	 * the {@link Column} the implementing ColumnDefinition would return via the
	 * build() method.
	 **/
	public boolean conformsToDefinition(String s);

	/**
	 * This method should convert the String value to the type held by the
	 * implementation of the ColumnDefinition.
	 **/
	public T build(String s) throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException;

}
