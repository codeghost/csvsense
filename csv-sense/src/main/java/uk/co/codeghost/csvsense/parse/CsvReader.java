/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import uk.co.codeghost.csvsense.parse.column.BooleanColumn;
import uk.co.codeghost.csvsense.parse.column.Column;
import uk.co.codeghost.csvsense.parse.column.definition.BooleanColumnDefinition;
import uk.co.codeghost.csvsense.parse.column.definition.ColumnDefinition;
import uk.co.codeghost.csvsense.parse.column.definition.DateColumnDefinition;
import uk.co.codeghost.csvsense.parse.column.definition.NumberColumnDefinition;
import uk.co.codeghost.csvsense.parse.column.definition.StringColumnDefinition;

/**
 * <p>
 * Provides mechanism to load text files with custom delimiter and quote
 * settings, parsing column values into typed
 * {@link uk.co.codeghost.csvsense.parse.column.Column} classes
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Oct 29, 2012
 */
public class CsvReader {

	private BufferedReader reader;
	private LineParser parser;

	/**
	 * The minimal constructor requires a valid {@link File} reference. Defaults
	 * to a comma delimiter and null quote configuration.
	 * 
	 * @param csv
	 *            A valid {@link File} reference to the CSV to be read.
	 * @throws FileNotFoundException
	 */
	public CsvReader(File csv) throws FileNotFoundException {
		reader = new BufferedReader(new FileReader(csv));
		parser = new LineParser();
	}

	/**
	 * Creates a <code>CsvReader</code> for the specified {@link File}
	 * reference. Defaults to a null quote configuration, but uses the specified
	 * delimiter.
	 * 
	 * @param csv
	 *            A valid {@link File} reference to the CSV to be read.
	 * @param delimiter
	 *            {@link char} to be used as the delimiter for splitting the
	 *            specified file
	 * @throws FileNotFoundException
	 */
	public CsvReader(File csv, char delimiter) throws FileNotFoundException {
		reader = new BufferedReader(new FileReader(csv));
		parser = new LineParser(delimiter);
	}

	/**
	 * Creates a <code>CsvReader</code> for the specified {@link File}
	 * reference, using specified delimiter and quote char's for parsing the
	 * file
	 * 
	 * @param csv
	 *            A valid {@link File} reference to the CSV to be read.
	 * @param delimiter
	 *            {@link char} to be used as the delimiter for splitting the
	 *            specified file
	 * @param quote
	 *            {@link char} to be used as the quote wrapper
	 * @throws FileNotFoundException
	 */
	public CsvReader(File csv, char delimiter, char quote)
			throws FileNotFoundException {
		reader = new BufferedReader(new FileReader(csv));
		parser = new LineParser(delimiter, quote);
	}

	/**
	 * <p>
	 * Allows the behaviour for parsing boolean values to be turned off / on
	 * where values that could represent boolean values are not intended.
	 * </p>
	 * <p>
	 * The default behaviour is for those values considered boolean flags by
	 * {@link BooleanColumn} to be converted as such and this flag should be
	 * switched off where that is not the intention.
	 */
	public void setParseBools(boolean parseBools) {
		parser.setParseBools(parseBools);
	}

	/**
	 * Retrieves the next line of the file as a {@link List} of String tokens
	 * 
	 * @throws IOException
	 */
	public List<String> nextLine() throws IOException {
		return parser.parse(reader.readLine());
	}

	/**
	 * <p>
	 * Retrieves the next line of the file as a {@link Row} or typed
	 * {@link Column}'s.
	 * </p>
	 * <p>
	 * Only the standard {@link ColumnDefinition}'s are applied in the order of
	 * <ol>
	 * <li>{@link NumberColumnDefinition}</li>
	 * <li>{@link DateColumnDefinition}</li>
	 * <li>{@link BooleanColumnDefinition}</li>
	 * <li>{@link StringColumnDefinition}</li>
	 * </ol>
	 * </p>
	 * 
	 * @throws IOException
	 *             , IllegalArgumentException, SecurityException,
	 *             InstantiationException, IllegalAccessException,
	 *             InvocationTargetException, NoSuchMethodExceptionException
	 */
	public Row nextRow() throws IOException, IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		String line = reader.readLine();
		if (line != null) {
			return parser.parseToRow(line);
		} else {
			return null;
		}
	}

	/**
	 * <p>
	 * Retrieves the next line of the file as a {@link Row} or typed
	 * {@link Column}'s based on the {@link ColumnDefinition}'s passed to the
	 * method.
	 * </p>
	 * <p>
	 * ColumnDefinition's will be applied to the tokens in the order of
	 * precendence they appear in the List and therefore where a value can be
	 * represented by more than one of the definitions, the ColumnDefinition
	 * earlier in the List will be the one applied.
	 * </p>
	 * 
	 * @throws IOException
	 *             , IllegalArgumentException, SecurityException,
	 *             InstantiationException, IllegalAccessException,
	 *             InvocationTargetException, NoSuchMethodExceptionException
	 */
	public Row nextRow(
			List<ColumnDefinition<? extends Column<?>>> columnDefinitions)
			throws IOException, IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		String line = reader.readLine();
		if (line != null) {
			return parser.parseToRow(line, columnDefinitions);
		} else {
			return null;
		}
	}

}
