/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse.column.definition;

import java.lang.reflect.InvocationTargetException;

import uk.co.codeghost.csvsense.parse.Row;
import uk.co.codeghost.csvsense.parse.column.Column;
import uk.co.codeghost.csvsense.parse.column.StringColumn;

/**
 * <p>
 * Class to encapsulate a String as a {@link Column} object when representing a
 * value in a {@link Row}.
 * </p>
 * <p>
 * Used as the default fallback Column definition.
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Nov 1, 2012
 */
public class StringColumnDefinition implements ColumnDefinition<StringColumn> {

	public boolean conformsToDefinition(String s) {
		return s != null;
	}

	public StringColumn build(String s) throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		return StringColumn.class.getConstructor(String.class).newInstance(s);
	}

}
