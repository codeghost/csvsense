/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse.column;

import org.apache.commons.lang.NullArgumentException;

/**
 * <p>
 * {@link Column} wrapper for {@link Number} objects.
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Oct 29, 2012
 */
public class NumberColumn implements Column<Number> {

	private final Number value;

	public NumberColumn(Number value) throws NullArgumentException {
		if (value == null)
			throw new NullArgumentException("value");

		this.value = value;
	}

	@SuppressWarnings("unchecked")
	public Class<Number> getColumnClass() {
		return (Class<Number>) value.getClass();
	}

	public Number getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value.toString();
	}

}
