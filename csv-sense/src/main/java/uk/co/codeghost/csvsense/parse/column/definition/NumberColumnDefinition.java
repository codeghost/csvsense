/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse.column.definition;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang.math.NumberUtils;

import uk.co.codeghost.csvsense.parse.column.NumberColumn;

/**
 * <p>
 * {@link ColumnDefinition} implementation to allow String's to be tested for
 * successful conversion to {@link Number} objects and convert them to their
 * corresponding {@link NumberColumn} object.
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Nov 1, 2012
 */
public class NumberColumnDefinition implements ColumnDefinition<NumberColumn> {

	/**
	 * Checks if the String parameter represents a {@link Number}
	 */
	public boolean conformsToDefinition(String s) {
		return NumberUtils.isNumber(s);
	}

	/**
	 * Converts the String parameter to a {@link Number} and wraps it in the
	 * corresponding {@link NumberColumn} object.
	 */
	public NumberColumn build(String s) throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		Number n = NumberUtils.createNumber(s);
		return NumberColumn.class.getConstructor(Number.class).newInstance(n);
	}

}
