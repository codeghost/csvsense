/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse.column.definition;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import uk.co.codeghost.csvsense.parse.column.BooleanColumn;

/**
 * <p>
 * {@link ColumnDefinition} implementation to allow String's to be tested for
 * successful parsing to {@link BooleanColumn}'s
 * 
 * The following values are considered valid String representations of Boolean:
 * <ul>
 * <li>TRUE</li>
 * <li>T</li>
 * <li>YES</li>
 * <li>Y</li>
 * <li>FALSE</li>
 * <li>F</li>
 * <li>NO</li>
 * <li>N</li>
 * </ul>
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Nov 1, 2012
 */
public class BooleanColumnDefinition implements ColumnDefinition<BooleanColumn> {

	private static final List<String> trueValues = Arrays.asList("TRUE", "T",
			"YES", "Y");
	private static final List<String> falseValues = Arrays.asList("FALSE", "F",
			"NO", "N");

	/**
	 * Checks if the uppercase version of the String parameter matches one of
	 * the recgnised Boolean representations and returns true if so.
	 */
	public boolean conformsToDefinition(String s) {
		return trueValues.contains(s.toUpperCase())
				|| falseValues.contains(s.toUpperCase());
	}

	/**
	 * Converts the String parameter to the appropriate Boolean and wraps it in
	 * an {@link BooleanColumn} or returns an {@link IllegalArgumentException}
	 */
	public BooleanColumn build(String s) throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		if (trueValues.contains(s.toUpperCase()))
			return BooleanColumn.class.getConstructor(Boolean.class)
					.newInstance(true);

		if (falseValues.contains(s.toUpperCase()))
			return BooleanColumn.class.getConstructor(Boolean.class)
					.newInstance(false);

		throw new IllegalArgumentException(
				"Argument is not recognised as a valid BooleanColumn constructor argument");
	}

}
