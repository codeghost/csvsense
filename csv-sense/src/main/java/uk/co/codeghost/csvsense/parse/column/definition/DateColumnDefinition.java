/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse.column.definition;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import uk.co.codeghost.csvsense.parse.column.DateColumn;

/**
 * <p>
 * Class description
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Nov 1, 2012
 */
public class DateColumnDefinition implements ColumnDefinition<DateColumn> {

	private static final String DEFAULT_DATE_FORMAT = "dd-MM-yy hh:mm:ss";

	private final SimpleDateFormat formatter;

	/**
	 * Creates a {@link DateColumnDefinition} object using the default date
	 * format "dd-MM'yy hh:mm:ss".
	 */
	public DateColumnDefinition() throws IllegalArgumentException {
		formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
	}

	/**
	 * Creates a {@link DateColumnDefinition} object using the format specified.
	 */
	public DateColumnDefinition(String format) throws NullPointerException,
			IllegalArgumentException {
		formatter = new SimpleDateFormat(format);
	}

	/**
	 * Checks if the String parameter can be successfully parsed to a date with
	 * the {@link SimpleDateFormat}ter.
	 */
	public boolean conformsToDefinition(String s) {
		try {
			Date date = formatter.parse(s);
			return date != null;
		} catch (ParseException e) {
			return false;
		}
	}

	/**
	 * <p>
	 * Converts the String parameter to a {@link Date} object and wraps it in a
	 * {@link DateColumn} object.
	 * </p>
	 */
	public DateColumn build(String s) throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		Date date = null;
		try {
			date = formatter.parse(s);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return DateColumn.class.getConstructor(Date.class).newInstance(date);
	}

}
