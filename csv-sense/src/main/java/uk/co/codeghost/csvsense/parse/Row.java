/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse;

import java.util.Enumeration;
import java.util.List;

import uk.co.codeghost.csvsense.parse.column.Column;

/**
 * <p>
 * Logical wrapper of a row of data split into {@link Column}'s
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Oct 29, 2012
 */
public class Row implements Enumeration<Column<?>> {

	private final List<Column<?>> columns;
	private int column = 0;

	Row(List<Column<?>> columns) {
		if (columns == null)
			throw new IllegalArgumentException("columns cannot be null");

		this.columns = columns;
	}

	public boolean hasMoreElements() {
		return column < columns.size();
	}

	public Column<?> nextElement() {
		return columns.get(column++);
	}

}
