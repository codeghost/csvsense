/**
 * <p>Copyright Codeghost Ltd.</p>
 *
 * @author <a href="http://www.codeghost.co.uk">Codeghost Ltd.</a>
 * 
 * This file is part of CsvSense.
 * 
 *  CsvSense is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  CsvSense is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CsvSense.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.codeghost.csvsense.parse;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import uk.co.codeghost.csvsense.parse.column.BooleanColumn;
import uk.co.codeghost.csvsense.parse.column.Column;
import uk.co.codeghost.csvsense.parse.column.definition.BooleanColumnDefinition;
import uk.co.codeghost.csvsense.parse.column.definition.ColumnDefinition;
import uk.co.codeghost.csvsense.parse.column.definition.DateColumnDefinition;
import uk.co.codeghost.csvsense.parse.column.definition.NumberColumnDefinition;
import uk.co.codeghost.csvsense.parse.column.definition.StringColumnDefinition;

/**
 * <p>
 * Implements the parsing of String lines read from a flat file to either a
 * {@link List} of String values or into typed
 * {@link uk.co.codeghost.csvsense.parse.column.Column} classes
 * </p>
 * 
 * @author <a href="mailto:matthew.bain@codeghost.co.uk">Matthew Bain</a>
 * @date Oct 29, 2012
 */
class LineParser {

	private static final char DEFAULT_DELIMITER = ',';
	private static final char DEFAULT_QUOTE = '\u0000';

	private final char delimiter;
	private final char quote;
	private boolean parseBools = true;

	/**
	 * Default constructor assumes comma ',' delimiter and null quote '\u0000'
	 */
	public LineParser() {
		this.delimiter = DEFAULT_DELIMITER;
		this.quote = DEFAULT_QUOTE;
	}

	/**
	 * Constructor which overrides the default delimiter with that specified.
	 */
	public LineParser(char delimiter) {
		this.delimiter = delimiter;
		this.quote = DEFAULT_QUOTE;
	}

	/**
	 * Constructor which overrides the default delimiter and quote characters
	 * with those specif1eid.
	 */
	public LineParser(char delimiter, char quote) {
		this.delimiter = delimiter;
		this.quote = quote;
	}

	/**
	 * <p>
	 * Method to allow the parser to be instructed whether or not to attempt to
	 * parse values that may represent Boolean's.
	 * </p>
	 * <p>
	 * By default parsing of Strings into {@link BooleanColumn} objects is
	 * turned on.
	 * </p>
	 */
	public void setParseBools(boolean parseBools) {
		this.parseBools = parseBools;
	}

	/**
	 * <p>
	 * Method to parse the specified String into String tokens.
	 * </p>
	 * 
	 * @param line
	 *            String to be parsed.
	 */
	public List<String> parse(String line) {
		List<String> tokens = new ArrayList<String>();
		boolean inQuotes = false;
		StringBuilder builder = new StringBuilder();
		for (char c : line.toCharArray()) {
			if (quote != DEFAULT_QUOTE && c == quote) {
				inQuotes = !inQuotes;
			} else {
				if (c != delimiter || inQuotes) {
					builder.append(c);
				} else {
					tokens.add(builder.toString());
					builder = new StringBuilder();
				}
			}
		}
		tokens.add(builder.toString());
		return tokens;
	}

	/**
	 * <p>
	 * Method to parse the specified String into {@link ColumnDefinition}'s as
	 * specified in the columnDefinitions argument.
	 * </p>
	 * 
	 * @param line
	 *            String to be parsed.
	 * @param columnDefinitions
	 *            {@link List} or {@link ColumnDefinition}'s to be used in the
	 *            parsing of the line.
	 */
	public Row parseToRow(String line,
			List<ColumnDefinition<? extends Column<?>>> columnDefinitions)
			throws IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		List<Column<?>> columns = new ArrayList<Column<?>>();
		for (String s : parse(line)) {
			for (ColumnDefinition<? extends Column<?>> definition : columnDefinitions) {
				if (definition.conformsToDefinition(s)) {
					columns.add(definition.build(s));
					break;
				}
			}
		}
		return new Row(columns);
	}

	/**
	 * <p>
	 * Method to parse the specified String into the default
	 * {@link ColumnDefinition}'s in the following order of precendence:
	 * <ol>
	 * <li>{@link NumberColumnDefinition}</li>
	 * <li>{@link DateColumnDefinition}</li>
	 * <li>{@link BooleanColumnDefinition}</li>
	 * <li>{@link StringColumnDefinition}</li>
	 * </ol>
	 * </p>
	 * 
	 * @param line
	 *            String to be parsed.
	 */
	public Row parseToRow(String line) throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		List<ColumnDefinition<? extends Column<?>>> columnDefinitions = new ArrayList<ColumnDefinition<? extends Column<?>>>();
		columnDefinitions.add(new NumberColumnDefinition());
		columnDefinitions.add(new DateColumnDefinition());
		if (parseBools)
			columnDefinitions.add(new BooleanColumnDefinition());
		columnDefinitions.add(new StringColumnDefinition());

		return parseToRow(line, columnDefinitions);
	}
}
